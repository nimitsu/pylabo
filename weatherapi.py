import os,requests

weather_api_key = str(os.environ.get("WEATHER_API_KEY",""))
weather_url="http://api.openweathermap.org/data/2.5/forecast"
weather_city_id=str(os.environ.get("WEATHER_CITY_ID",""))
def request():
    url = weather_url + "?appid={a}&id={b}".format(a=weather_api_key, b=weather_city_id)
    print(url)
    r = requests.get(url)
    return r.json()

