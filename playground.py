import numpy as np
import matplotlib.pyplot as plt

rng = np.arange(0, 9, 0.1)
y1 = np.sin(rng)
y2 = np.cos(rng)

#グラフの描画
plt.plot(rng, y1, label="sin")
plt.plot(rng, y2, linestyle = "--", label = "cos")
plt.xlabel("x")
plt.ylabel("y")
plt.title('sin & cos')
plt.legend()
plt.show()
