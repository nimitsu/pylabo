import os,requests,random

def gasreq(model_name):
    gas_url=str(os.environ.get("MASTER_GAS_URL", ""))
    rnd = random.randint(1,1000000)
    url = gas_url + "?sheetName={a}&id={b}".format(a=model_name, b=rnd)
    print(url)
    r = requests.get(url)
    print(r)
    return r