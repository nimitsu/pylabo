import os, weatherapi, requests
from datetime import datetime
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookParser
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)

app = Flask(__name__)

channel_access_token = str(os.environ.get('LINE_CHANNEL_ACCESS_TOKEN',''))
channel_secret = str(os.environ.get('LINE_CHANNEL_SECRET',''))

line_bot_api = LineBotApi(channel_access_token)
parser = WebhookParser(channel_secret)

def checkweather(strweather):
    charweather = ""
    if(strweather == "clear sky" or strweather == "few clouds"):
        charweather = chr(0x1000A9)
    elif ( \
    strweather == "scattered clouds" or \
    strweather == "broken clouds" or \
    strweather == "overcast clouds"\
    ):
        charweather = chr(0x1000AC)
    elif ( \
    strweather == "shower rain" or \
    strweather == "rain" or \
    strweather == "light rain"
    ):
        charweather = chr(0x1000AA)
    elif (strweather == "thunderstorm"):
        charweather = chr(0x10003A)
    elif (strweather == "snow"):
        charweather = chr(0x1000AB)
    return charweather
    
@app.route('/')
def main():
    return 'Hello World!!!!!'

@app.route("/weather")
def weather():
    todayDate = datetime.today()
    res = weatherapi.request()
    rslt = """今日のお天気だよ"""
    cnt = 0
    limitcnt = 5
    kelvin = 273.15
    for val in res['list']:
        #日付
        dt=datetime.strptime(val['dt_txt'], '%Y-%m-%d %H:%M:%S')
        #if(todayDate.day != dt.day):
        #    continue
        print(dt)
        #天気
        wt=val['weather'][0]['description']
        chwt = checkweather(wt)
        #気温
        tm=val['main']['temp']
        tm-=kelvin
        #最高気温
        tmax=val['main']['temp_max']
        tmax-=kelvin
        #最低気温
        tmin=val['main']['temp_min']
        tmin-=kelvin
        #降水量
        rslt+="""
{today_day}日
{today_hour}時からは
{wt}{chwt}
最低気温は {tmin}
最高気温は {tmax}
        """.format(today_day=str(dt.day), today_hour=str(dt.hour),wt=wt, chwt=chwt,tmin=round(tmin,1),tmax=round(tmax,1))
        cnt+=1
        if(cnt>limitcnt):
            break
    
    rslt+="Have a good day"
    print(rslt)

    group_id = str(os.environ.get('TENT_GROUP_ID',''))
    try:
        line_bot_api.push_message(group_id, TextSendMessage(text=rslt))
    except LineBotApiError as e:
        print(e)
    return str(rslt)


    
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    print(body)
    # parse webhook body
    try:
        events = parser.parse(body, signature)
    except InvalidSignatureError:
        abort(400)
        
    # if event is MessageEvent and message is TextMessage, then echo text
    for event in events:
        if not isinstance(event, MessageEvent):
             continue
        if not isinstance(event.message, TextMessage):
            continue
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=event.message.text)
            
        )
    return 'OK'

@app.route("/gas")
def gas():
    model_name = request.args.get("sheetName","MasterCharacterModel")
    print(model_name)
    # model_name = "MasterCharacterModel"
    gas_url=str(os.environ.get("MASTER_GAS_URL",""))
    url = gas_url + "?sheetName={a}&id={b}".format(a=model_name, b=1)
    print(url)
    r = requests.get(url)
    print(r.text)
    return r.text
    # val = request.args.get("sheetName", "MasterCharecterModel")
    # print(val)
    # res = mastergas.gasreq(val)
    # print(res)
    # return res

if __name__ == '__main__':
    app.run()
